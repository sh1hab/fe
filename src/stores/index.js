
import { defineStore } from 'pinia'

export const useCounterStore = defineStore('login', {
    state: () => ({ token: null }),
    getters: {
        getToken: (state) => state.token,
    },
    actions: {
        setToken(token) {
            this.token = token
        },
    },
})