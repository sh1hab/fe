
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
// import Vue from 'vue'
// import VueRouter from 'vue-router'

// import { createRouter, createWebHashHistory } from 'vue-router';

// import Home from './components/Home.vue'
// import About from './components/About.vue'


// const routes = [
//     { path: '/', component: Home },
//     { path: '/about', component: About },
// ]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.

// const router = createRouter({
//     // 4. Provide the history implementation to use. We
//     // are using the hash history for simplicity here.
//     history: createWebHashHistory(),
//     routes, // short for `routes: routes`
// })

const pinia = createPinia()

const app = createApp(App)

app.use(router)
app.use(pinia)

app.mount('#app')

// createApp(App).mount('#app')
